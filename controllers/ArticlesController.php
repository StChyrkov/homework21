<?php


class ArticlesController extends Controller
{
    public function actionIndex()
    {
        $articles = Article::findAll();
        $this->view->render(
            'articles/index',
            ['articles' => $articles]
        );
    }

    public function actionView($id = 0)
    {
        $article = Article::findOne((int) $id);
        $this->view->render('articles/view', ['article' => $article]);
    }
}