<?php

class SiteController extends Controller
{
    public function actionIndex()
    {
        $this->view->render('site/index');
    }

    public function actionContacts()
    {
        $this->view->render('site/contacts', ['title' => 'Контакты:']);
    }
}