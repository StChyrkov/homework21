<?php

class BlogController extends Controller
{

    public function actionIndex()
    {
        $posts = Post::findAll();
        $this->view->render(
            'blog/index',
            ['posts' => $posts]
        );
    }

    public function actionPost($id = 0)
    {
        $post = Post::findOne((int) $id);
        $this->view->render('blog/post', ['post' => $post]);
    }
}