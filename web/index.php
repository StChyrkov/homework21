<?php

require_once '../components/Model.php';
require_once '../models/Post.php';
require_once '../models/Article.php';
require_once '../components/Controller.php';
require_once '../components/View.php';
require_once '../components/Application.php';

(new Application)->run();
