<?php

class Post extends Model


{
    protected $id;
    protected $short;
    protected $content;

    public function getId()
    {
        return $this->id;
    }

    public function getShort()
    {
        return $this->short;
    }

    public function getContent()
    {
        return $this->content;
    }

    public static function findAll()
    {
        $connect = new Post;

        $sql = 'select * from posts';

        $query = $connect->db->prepare($sql);

        $query->execute();

        $rows = $query->fetchAll();

        $posts = [];

        foreach ($rows as $row){
            $post = new Post;
            $post->id = $row['id'];
            $post->short = $row['short'];
            $post->content = $row['content'];
            $posts[] = $post;
        }

        return $posts;
    }

    public static function findOne($id)
    {
        $connect = new Post;

        $sql = 'select * from posts where id=:id';

        $query = $connect->db->prepare($sql);

        $query->bindValue(':id', $id);

        $query->execute();

        $row = $query->fetch();

        $post = new Post;
        $post->short = $row['short'];
        $post->content = $row['content'];

        return $post;
    }
}