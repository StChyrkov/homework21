<?php


class Article extends Model
{
    protected $id;
    protected $title;
    protected $text;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getText()
    {
        return $this->text;
    }

    public static function findAll()
    {
        $connect = new Article;

        $sql = 'select * from articles';

        $query = $connect->db->prepare($sql);

        $query->execute();

        $rows = $query->fetchAll();

        $articles = [];

        foreach ($rows as $row){
            $article = new Article;
            $article->id = $row['id'];
            $article->title = $row['title'];
            $article->text = $row['text'];
            $articles[] = $article;
        }

        return $articles;
    }

    public static function findOne($id)
    {
        $connect = new Article;

        $sql = 'select * from articles where id=:id';

        $query = $connect->db->prepare($sql);

        $query->bindValue(':id', $id);

        $query->execute();

        $row = $query->fetch();

        $article = new Article;
        $article->title = $row['title'];
        $article->text = $row['text'];

        return $article;
    }
}