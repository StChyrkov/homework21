<h1>Лучшие статьи на сегодня</h1>
<table>
    <tbody>
    <?php foreach ($articles as $article) : ?>
        <tr>
            <td><?=$article->getTitle() ?></td>
            <td><a href="/articles/view/<?=$article->getId() ?>" class="btn btn-primary">Читать подробнее</a></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>