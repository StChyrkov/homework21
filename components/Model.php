<?php


abstract class Model
{
    public $db;

    public function __construct()
    {
        $this->db = new PDO(
            'mysql:host=localhost;dbname=sportsblog',
            'sportsblog_user',
            '23021989'
        );

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

}